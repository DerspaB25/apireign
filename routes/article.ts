import {Router} from 'express';
import { createArticles, deleteArticle, getArticles } from '../controllers/article';


const router = Router();


router.get('/', getArticles);
router.post('/', createArticles)
router.delete('/:id', deleteArticle)

export default router;