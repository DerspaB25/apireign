import {Request, Response} from 'express';
import axios from 'axios';
import {Article} from '../models/article';
import { cleanFilters } from '../helpers/cleanFilters';


export const getArticles  = async (req: Request, res: Response) => {
   
    const {pageIndex, ...filters} = req.query

    const arrFilters = cleanFilters(filters);

    const currentPage = String(pageIndex);

    const options = {
        page: parseInt(currentPage) ||  1,
        limit: 5
    };

    try {
        const items = await Article.paginate(arrFilters.length > 0 ? { $and: arrFilters }: {}, options);

        
        if(items.totalPages < parseInt(currentPage) ) {
            
            res.status(400).json({
                msg: `The page ${pageIndex} doesn't exist`
            })
            
        }
        if(items.docs.length <= 0){
            res.status(404).json({
                msg: "doesn't exist an article with this filters"
            })
        }
        else {
            res.json({
                items: items.docs,
                page: items.page,
                totalPage: items.totalPages
            })
        }

        
        
    } catch (error) {
        
    }


}

export const createArticles = async (req: Request, res: Response) => {

    try {
        const articles = await axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs');

        
        const options = { upsert: true, new: true, setDefaultsOnInsert: true };
        

        articles.data.hits.map( async (article: any) => {
            
            await Article.findOneAndUpdate({parent_id: article.parent_id}, article , options);
        })

        res.json({
            msg: 'articles inserted success'
        })

    } catch (error) {
        res.status(400).json({
            msg: 'Error to insert articles to database',
            error
        })  
        
    }
}

export const deleteArticle = async (req: Request, res: Response) => {

    const {id} = req.params

    try {
        
        await Article.deleteOne({_id: id})
        res.status(200).json({
            msg: `Article with id: ${id} delete succes`
        })

    } catch (error) {
        res.json({
            msg: `Error to delete the articles with id: ${id}`,
            error
        })
    }

}