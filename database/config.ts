
import mongoose from 'mongoose';


const dbConnection =  async () => {

    try {
        
        await mongoose.connect(process.env.MONGODB_CNN || '')
        console.log('Database is online');
    } catch (error) {

        console.log(error);
        throw new Error('Error to connect to database');
    }

}


export default dbConnection;