

export const cleanFilters = (filters: any): Object[] =>  {


    let arrFilters: Object[] = []

    for (const filter in filters) {
        if(filter === 'title' ){
            arrFilters.push({'story_title': filters[filter]})
        }
         if(filter === 'tags'){
            arrFilters.push({'_tags': {$all: String(filters[filter]).split(',')}})
        }
         if(filter === 'author'){
            arrFilters.push({[filter]: filters[filter]})
        }
    }

    return arrFilters;

}
