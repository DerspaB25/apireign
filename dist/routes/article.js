"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const article_1 = require("../controllers/article");
const router = (0, express_1.Router)();
router.get('/', article_1.getArticles);
router.post('/', article_1.createArticles);
router.delete('/:id', article_1.deleteArticle);
exports.default = router;
//# sourceMappingURL=article.js.map