"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteArticle = exports.createArticles = exports.getArticles = void 0;
const axios_1 = __importDefault(require("axios"));
const article_1 = require("../models/article");
const cleanFilters_1 = require("../helpers/cleanFilters");
const getArticles = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const _a = req.query, { pageIndex } = _a, filters = __rest(_a, ["pageIndex"]);
    const arrFilters = (0, cleanFilters_1.cleanFilters)(filters);
    const currentPage = String(pageIndex);
    const options = {
        page: parseInt(currentPage) || 1,
        limit: 5
    };
    try {
        const items = yield article_1.Article.paginate(arrFilters.length > 0 ? { $and: arrFilters } : {}, options);
        if (items.totalPages < parseInt(currentPage)) {
            res.status(400).json({
                msg: `The page ${pageIndex} doesn't exist`
            });
        }
        if (items.docs.length <= 0) {
            res.status(404).json({
                msg: "doesn't exist an article with this filters"
            });
        }
        else {
            res.json({
                items: items.docs,
                page: items.page,
                totalPage: items.totalPages
            });
        }
    }
    catch (error) {
    }
});
exports.getArticles = getArticles;
const createArticles = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const articles = yield axios_1.default.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs');
        const options = { upsert: true, new: true, setDefaultsOnInsert: true };
        articles.data.hits.map((article) => __awaiter(void 0, void 0, void 0, function* () {
            yield article_1.Article.findOneAndUpdate({ parent_id: article.parent_id }, article, options);
        }));
        res.json({
            msg: 'articles inserted success'
        });
    }
    catch (error) {
        res.status(400).json({
            msg: 'Error to insert articles to database',
            error
        });
    }
});
exports.createArticles = createArticles;
const deleteArticle = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        yield article_1.Article.deleteOne({ _id: id });
        res.status(200).json({
            msg: `Article with id: ${id} delete succes`
        });
    }
    catch (error) {
        res.json({
            msg: `Error to delete the articles with id: ${id}`,
            error
        });
    }
});
exports.deleteArticle = deleteArticle;
//# sourceMappingURL=article.js.map