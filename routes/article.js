"use strict";
exports.__esModule = true;
var express_1 = require("express");
var article_1 = require("../controllers/article");
var router = (0, express_1.Router)();
router.get('/', article_1.getArticles);
router.post('/', article_1.createArticles);
exports["default"] = router;
