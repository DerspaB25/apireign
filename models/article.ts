import {Schema, model, Document, PaginateModel} from 'mongoose';
import  mongoosePaginate from 'mongoose-paginate-v2';


interface  IArticle extends Document {
    created_at: String,
    author: String
    comment_text: String
    story_id: String
    story_title: String
    parent_id: Number
    created_at_i: Number
}

 const ArticleSchema = new Schema({
    created_at: {
        type: String,
        required: [true, 'The created date is required']
    },
    author: {
        type: String,
        required: [true, 'The author is required']
    },
    comment_text: {
        type: String,
        required: [true, 'The comment is required']
    },
    story_id: {
        type: Number,
        required: [true, 'The story id is required']
    },
    story_title: {
        type: String,
        required: [true, 'The story title is required']
    },
    parent_id: {
        type: Number,
        required: [true, 'The parent id is required'],
        unique: true
    },
    created_at_i: {
        type: Number,
        required: [true, 'The created at id is required']
    },
    _tags: [String]


});

ArticleSchema.plugin(mongoosePaginate)

interface ArticleModel<T extends Document> extends PaginateModel<T> {}

export const Article: ArticleModel<IArticle> = model<IArticle>('Article', ArticleSchema) as ArticleModel<IArticle>;