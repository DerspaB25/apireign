
import supertest from 'supertest';
import request from 'supertest';
import app from '../app';


describe("Post articles", () => {

    describe("Request to insert articles", () => {
        test("should respond with a 200 status code", async () => {
            const response = await request(app.getApp()).post('/api/articles').send({})
            expect(response.statusCode).toBe(200)
        })
    })

})

describe("Get articles", () => {

    describe("Request to get articles for the first page", () => {
        test("should respond with a 200 status code", async () => {
            const response = await request(app.getApp()).get('/api/articles').send({})
            expect(response.statusCode).toBe(200)
        })
    })

})

