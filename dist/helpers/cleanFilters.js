"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cleanFilters = void 0;
const cleanFilters = (filters) => {
    let arrFilters = [];
    for (const filter in filters) {
        if (filter === 'title') {
            arrFilters.push({ 'story_title': filters[filter] });
        }
        if (filter === 'tags') {
            arrFilters.push({ '_tags': { $all: String(filters[filter]).split(',') } });
        }
        if (filter === 'author') {
            arrFilters.push({ [filter]: filters[filter] });
        }
    }
    return arrFilters;
};
exports.cleanFilters = cleanFilters;
//# sourceMappingURL=cleanFilters.js.map