"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Article = void 0;
const mongoose_1 = require("mongoose");
const mongoose_paginate_v2_1 = __importDefault(require("mongoose-paginate-v2"));
const ArticleSchema = new mongoose_1.Schema({
    created_at: {
        type: String,
        required: [true, 'The created date is required']
    },
    author: {
        type: String,
        required: [true, 'The author is required']
    },
    comment_text: {
        type: String,
        required: [true, 'The comment is required']
    },
    story_id: {
        type: Number,
        required: [true, 'The story id is required']
    },
    story_title: {
        type: String,
        required: [true, 'The story title is required']
    },
    parent_id: {
        type: Number,
        required: [true, 'The parent id is required'],
        unique: true
    },
    created_at_i: {
        type: Number,
        required: [true, 'The created at id is required']
    },
    _tags: [String]
});
ArticleSchema.plugin(mongoose_paginate_v2_1.default);
exports.Article = (0, mongoose_1.model)('Article', ArticleSchema);
//# sourceMappingURL=article.js.map