"use strict";
exports.__esModule = true;
exports.cleanFilters = void 0;
var cleanFilters = function (filters) {
    var _a;
    var arrFilters = [];
    for (var filter in filters) {
        if (filter === 'title') {
            arrFilters.push({ 'story_title': filters[filter] });
        }
        if (filter === 'tags') {
            arrFilters.push({ '_tags': { $all: String(filters[filter]).split(',') } });
        }
        if (filter === 'author') {
            arrFilters.push((_a = {}, _a[filter] = filters[filter], _a));
        }
    }
    return arrFilters;
};
exports.cleanFilters = cleanFilters;
