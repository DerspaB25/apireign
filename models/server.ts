import express, {Application} from 'express';
import articleRoutes from '../routes/article';
import cors from 'cors';
import dbConnection from '../database/config';






class Server {

    private app: Application;
    private port: string;
    private apiPaths = {
        articles: '/api/articles'
    }

    constructor() {
        this.app = express();
        this.port = process.env.PORT || '8000';
        this.databaseConnection();
        this.middlewares();

        // routes definition
        this.routes()
    }

    async databaseConnection(){
        await dbConnection();
    }

    middlewares(){
        
        // CORS
        this.app.use( cors() ); 

        // Read of body
        this.app.use( express.json() );

    }

    routes(){
        this.app.use( this.apiPaths.articles, articleRoutes)
    }


    listen(){
        this.app.listen(this.port, () => {
            console.log(`Servidor corriendo en puerto: ${this.port}`);
        })
    }

    getApp(){
        return this.app
    }
}

export default Server;

